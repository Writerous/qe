export const tablesProcessor = {
    methods: {
        prepareWideTables(tables) {
            const newTables = [];

            for (let i = 0; i < tables.length; i += 2) {
                const table1 = tables[i];
                const table2 = tables[i + 1];
                const newTable = {
                    courseTitle: table1.courseTitle,
                    courseId: table1.courseId,
                    title1: table1.title,
                    title2: table2.title,
                    items: []
                };

                for (let j = 0; j < Math.max(table1.items.length, table2.items.length); j++) {
                    newTable.items[j] = {
                        mat1: (table1.items[j] && table1.items[j].mat) || '',
                        h1: (table1.items[j] && table1.items[j].h) || '',
                        primaryKey1: this.getPrimaryKey(table1.courseId, 1, j),
                        id1: (table1.items[j] && table1.items[j].id) || '',
                        mat2: (table2.items[j] && table2.items[j].mat) || '',
                        h2: (table2.items[j] && table2.items[j].h) || '',
                        primaryKey2: this.getPrimaryKey(table1.courseId, 2, j),
                        id2: (table2.items[j] && table2.items[j].id) || '',
                    };
                }

                newTables[i / 2] = newTable;
            }

            return () => Object.freeze(newTables);
        },
        prepareThinTables(tables) {
            const newTables = tables.map((table, i) => {
                return {
                    ...table,
                    items: table.items.map((item, j) => {
                        return {
                            ...item,
                            primaryKey: this.getPrimaryKey(table.courseId, (i + 1) % 2 + 1, j),
                        }
                    })
                }
            });
            return () => Object.freeze(newTables);
        },
        getPrimaryKey(courseId, half, index) {
            return [courseId, half, index].join('-');
        }
    }
}
