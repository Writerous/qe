import Vue from 'vue'
import VueCompositionAPI, { createApp, h } from '@vue/composition-api'
import QeTable from './components/table.vue'
import QeGraph from './components/graph.vue'

import App from './App.vue'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import VueRouter from 'vue-router'

// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(VueCompositionAPI)

Vue.use(VueRouter)

const routes = [
  { path: '/graph', component: QeGraph },
  { path: '', component: QeTable },
  { path: '/courses/:year', component: QeTable, props: true },
]

const router = new VueRouter({
  routes // short for `routes: routes`
})

const app = createApp({
  router,
  render: () => h(App)
})

app.mount('#app')
